// Requirements:
// - New engines are being added:
//  - V2Engine
//  - TurboEngine

// How would you use DIP to modify the code?

public class FormulaOneCarExample {
    private String model;
    private FormulaOneCarEngine formulaOneEngine;

    public FormulaOneCarExample() {}

    public void checkEngine() {
        boolean flag = formulaOneEngine.check();
        Data[] params = formulaOneEngine.getEngineParameters();
    }
}