/**
 * Single Responsibility Principle.
 */

package com.crio.qcalc;

public class RaceCar {

    private ArrayList<String> sensors;

    public boolean isSensorsWorking() {
        boolean isWorking;
 
        for (String sensor : sensors) {
            if (sensor.equalsIgnoreCase("GYRO")) {
                isWorking = isWorking && checkGyroSensor();
            }
 
            if (sensor.equalsIgnoreCase("FUEL")) {
                isWorking = isWorking && checkFuelSensor();
            }
         }
 
        return isWorking;
    }
 
    private boolean checkGyroSensor() {
        // ...
        // code to check if sensors are working
        // ...
        return false;
    }
 
    private boolean checkFuelSensor() { 
        // … 
    }
 
 // … rest of the class
}