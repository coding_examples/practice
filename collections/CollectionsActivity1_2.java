import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Java Collections Practice 1.2
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity1_2 {

    public static void main(String[] args) {
        Collection<Integer> collection = new ArrayList<Integer>();
        collection.addAll(Arrays.asList(1,4,6,9,3));
        
        // Create an iterator using the iterator() method
        Iterator<Integer> it = collection.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

}