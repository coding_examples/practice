import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Java Collections Practice 1.1
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity1_1 {

    public static void main(String[] args) {
        // The "type" of the group of elements we chosen is ArrayList.
        Collection<Integer> collection = new ArrayList<Integer>();

        // Initialize it using addAll() method and the Arrays.asList() helper.
        // You can also choose to use add() method, but it requires a little more typing.
        // It is also worth exploring the "Collections" helper class.
        collection.addAll(Arrays.asList(1,4,6,9,3));

        System.out.println(collection);
    }

}