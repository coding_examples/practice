# Pratice code examples during sessions
Code examples, questions, solutions etc as part of sessions and recordings delivered to users.

### Note
For Java: If you are using VSCode, remember to open the folder directly containing the files, otherwise you might get a package error.
