package oop;

public class Activity_2_1 {
    // Calculator class

    /* TODO:
     * 1. Put yourself in the shoes of the client. Is it obvious to you what this class does looking at it from the outside?
     * 2. Use Abstraction to improve it. 
     *  Hint 1: What are the methods that make up the calculator behaviour?
     *  Hint 2: Use interfaces.
     * 3. You can use Encapsulation if you see fit as well.
     * 4. Fix any other mistakes you can see.
     */

    public int result;

    public Activity_2_1() {
        this.result = -1;
    }

    public void perform(int a, int b, String op) {
        if (op.equals("+")) {
            result = a + b;
        }
        if (op.equals("-")) {
            result = a - b;
        }
        if (op.equals("*")) {
            result = 0;
            for (int i=0; i<b; i++) {
                result += a;
            }
        }
        if (op.equals("/")) {
            result = a / b;
        }
    }
}
