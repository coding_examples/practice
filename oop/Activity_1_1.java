package oop;

import java.util.ArrayList;

public class Activity_1_1 {

    /*
     * TODO:
     * - Do you see problems with the code below?
     * - How will encapsulation help?
     * - What methods will you choose to keep / discard in the encapsulated version?
     */

    static ArrayList<Integer> tvChannelNumbers = new ArrayList<Integer>();
    static ArrayList<Integer> radioChannelNumbers = new ArrayList<Integer>();
    int currentTVChannel = -1;
    int currentRadioChannel = -1;
    int TVVolume = -1;
    int RadioVolume = -1;

    public static void main(String[] args) {
        for(int i=0; i<10; i++) {
            tvChannelNumbers.add(i+1);
            radioChannelNumbers.add(i+1);
        }

        // Assume code for checking args[] and invoking methods below
        // ..
        // .. code
        // ..
    }

    void nextChannel(String deviceType) {
        if (deviceType.equals("TV")) {
            currentTVChannel += 1;
        }
    }

    void prevChannel(String deviceType) {
        if (deviceType.equals("TV")) {
            currentTVChannel -= 1;
        }
    }

    void setChannel(int channelNumber, String deviceType) {
        if (deviceType.equals("TV")) {
            for (int i=0; i < tvChannelNumbers.size(); i++) {
                if (channelNumber == tvChannelNumbers.get(i)) {
                    currentTVChannel = channelNumber;
                }
            }
        } else if (deviceType.equals("Radio")) {
            for (int i=0; i < radioChannelNumbers.size(); i++) {
                if (channelNumber == radioChannelNumbers.get(i)) {
                    currentRadioChannel = channelNumber;
                }
            }
        }
    }
    
}
