function example() {
    var x = 10;
    // What happens when the above is changed to let x = 10; ? Can you try it out?

    if (x == 10) {
        var x = 20;
    }

    return x;
}

x = example();
console.log(x);