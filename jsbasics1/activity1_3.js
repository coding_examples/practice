const arr = [1,2,3];
arr[0] = 5;
console.log(arr);

// What happens when you assign it?
// arr = [5, 2, 3];
// console.log(arr);